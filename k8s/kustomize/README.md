## Deployment using kustomize

Build and apply k8s manifests: `kustomize build overlays/<env> | kubectl apply -f -`
