CREATE TABLE IF NOT EXISTS events (
    id SERIAL PRIMARY KEY,
    title varchar(100) NOT NULL,
    description TEXT NOT NULL,
    image VARCHAR(80) NOT NULL,
    dates SMALLINT[] NOT NULL,
    month SMALLINT NOT NULL,
    year SMALLINT NOT NULL,
    datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    chat_id VARCHAR(20) NOT NULL,
    username VARCHAR(20),
    firstname VARCHAR(20),
    receive_notifications BOOL DEFAULT 'f',
    datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (chat_id)
);

CREATE TABLE IF NOT EXISTS notifications (
    id SERIAL PRIMARY KEY,
    content TEXT,
    is_sended BOOL DEFAULT 'f',
    sended_at TIMESTAMP,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS commands (
    id SERIAL PRIMARY KEY,
    chat_id VARCHAR(20) NOT NULL,
    command VARCHAR(20),
    datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (chat_id) REFERENCES users(chat_id)
);
