import requests
import subprocess
from logger import logger
from config import Config
from calendar import month_name
from datetime import datetime as dt


config = Config()

NL = ".\n"
BOT_TOKEN = config.BOT_TOKEN


def format_message_day(data):
    dates = f"{', '.join(str(item) for item in data['dates'])}"
    image_url = f"http://www.seasky.org/astronomy/assets/images/{data['image']}"
    month = return_name_of_month(dt.now().month)
    description_formatted = data["description"].replace(". ", NL)

    return f"""\n📅 *{dates} of {month}* [ ]({image_url})\n
*{data['title']}*\n
💬 {description_formatted}\n"""


def format_message_month(data):
    dates = f"{', '.join(str(item) for item in data['dates'])}"
    month = return_name_of_month(dt.now().month)

    return f"""📅 *{dates}* - *{data['title']}*\n\n"""


def return_name_of_month(month_number):
    return month_name[month_number]
