import sys
import requests
import traceback
from config import Config
from logger import logger
from functools import wraps
from database import Database
from datetime import datetime as dt
from telegram import Update, ParseMode, ChatAction
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    CallbackContext,
)

from utils import return_name_of_month, format_message_day, format_message_month

db = Database()
config = Config()

API_URL = config.API_URL
BOT_TOKEN = config.BOT_TOKEN
DEV_CHAT_ID = config.CHAT_ID

URL_SEASKY = f"http://www.seasky.org/astronomy/astronomy-calendar-{dt.now().year}.html"

ERROR_MESSAGE = """😭 Ops! Something is wrong!
I'm sending this error to my developer.
Sorry for this incovenient, please try again later."""


def send_typing_action(f):
    """Sends typing action while processing func command."""

    @wraps(f)
    def command_func(update, context, *args, **kwargs):
        context.bot.send_chat_action(
            chat_id=update.effective_message.chat_id, action=ChatAction.TYPING
        )
        return f(update, context, *args, **kwargs)

    return command_func


def save_info(f):
    """Save info from user"""

    @wraps(f)
    def insert_data(update, *args, **kwargs):
        logger.info(
            f"""Username: {update.message.chat.username},
First Name: {update.message.chat.first_name}
Command: {update.message.text},
Datetime: {update.message.date}"""
        )

        user_info = {
            "chat_id": update.message.chat.id,
            "message": update.message.text,
            "username": update.message.chat.username,
            "firstname": update.message.chat.first_name,
        }

        db.insert_user_info(user_info)
        return f(update, *args, **kwargs)

    return insert_data


@save_info
@send_typing_action
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    username = update.message.chat.username
    first_name = update.message.chat.first_name

    user = username if username is not None else first_name

    content = f"""
Hello *{user}*!
Welcome to ✨*Astronomical Events Bot*✨.
With this bot you can check astronomical events for current day.
Avaliable commands:
/start - Initialize bot
/today - Show events for today
/month - Show all events for current month
/notifications - Enable/Disable bot notifications
/statistics - Show bot statistics"""

    update.message.reply_text(content, parse_mode=ParseMode.MARKDOWN)


@save_info
@send_typing_action
def check_events_day(update: Update, context: CallbackContext) -> None:
    """Check and send astronomic events for today"""
    content_body = ""
    current_day = dt.now().day

    res = requests.get(f"{API_URL}/api/v1/events/filter_by?day={current_day}")

    if res.status_code == 200:
        res = res.json()
        data = res["content"]

        if len(data) > 0:
            for item in data:
                content_body += format_message_day(item)

            content = f"""*✨Astronomical Events✨*
{content_body}\n🔗 [From: Seasky]({URL_SEASKY})"""

        else:
            content = "❗️ Sorry, we don't have events to show today."

    else:
        content = ERROR_MESSAGE

    update.message.reply_text(
        content, parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True
    )


@save_info
@send_typing_action
def check_events_month(update: Update, context: CallbackContext) -> None:
    """Check and send astronomic events for current month"""
    content_body = ""
    current_month = dt.now().month
    month = return_name_of_month(current_month)

    res = requests.get(f"{API_URL}/api/v1/events/filter_by?month={current_month}")

    if res.status_code == 200:
        res = res.json()
        data = res["content"]

        if len(data) > 0:
            for item in data:
                content_body += format_message_month(item)

            content = f"""*✨Astronomical Events✨*\n
📅 *{month} Events*\n
{content_body}🔗 [From: Seasky]({URL_SEASKY})"""

        else:
            content = "❗️ Sorry, we don't have events to show."

    else:
        content = ERROR_MESSAGE

    update.message.reply_text(
        content, parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True
    )


@save_info
@send_typing_action
def toogle_user_notifications(update: Update, context: CallbackContext) -> None:
    chat_id = update.message.chat.id

    check_notifications = db.toogle_user_notifications(chat_id)

    if check_notifications is True:
        content = "*Yeah!* You can receive notifications now."
    elif check_notifications is False:
        content = "*Ahhh!* You don't receive notifications anymore."
    else:
        content = "*Ops!* I can't check your current status."

    update.message.reply_text(content, parse_mode=ParseMode.MARKDOWN)


@save_info
@send_typing_action
def send_bot_statistics(update: Update, context: CallbackContext) -> None:
    res = requests.get(f"{API_URL}/api/v1/users/statistics")

    if res.status_code == 200:
        res = res.json()
        count_all_users = res["content"]["count_all_users"]
        count_all_commands = res["content"]["count_all_commands"]
        count_users_with_notifications_enabled = res["content"][
            "count_users_with_notifications_enabled"
        ]

        content = f"""*✨Astronomical Events✨*\n
📈 *Statistics* \n
*All users*: {count_all_users}
*Users with notifications activated*: {count_users_with_notifications_enabled}
*All commands triggered*: {count_all_commands}
        """

    else:
        content = ERROR_MESSAGE

    update.message.reply_text(content, parse_mode=ParseMode.MARKDOWN)


@save_info
@send_typing_action
def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    content = """*✨Astronomical Events Bot✨*
With this bot you can check astronomical events for current day.
Avaliable commands:
/start - Initialize bot
/today - Show events for today
/month - Show events for current month
/notifications - Enable/Disable bot notifications
/statistics - Show bot statistics"""

    update.message.reply_text(content, parse_mode=ParseMode.MARKDOWN)


@save_info
@send_typing_action
def not_found_command(update: Update, context: CallbackContext):
    """Send a message when the command /help is issued."""

    content = """*✨Astronomical Events Bot✨*
Ops! Command not found.
Avaliable commands:
/start - Initialize bot
/today - Show events for today
/month - Show events for current month
/notifications - Enable/Disable bot notifications
/statistics - Show bot statistics"""

    update.message.reply_text(content, parse_mode=ParseMode.MARKDOWN)


def error(update: Update, context: CallbackContext):
    """Log all errors"""
    trace_error = "".join(traceback.format_tb(sys.exc_info()[2]))

    error_text = f"""Hey dev, something is wrong:\n
Error:\n<code>{context.error}</code>\n
Update:\n{update}.\n
The full traceback:\n<code>{trace_error}</code>"""

    logger.warning(
        f"Context: {context.error}, Update: {update}, Trace error: {trace_error}"
    )

    # context.bot.send_message(DEV_CHAT_ID, error_text, parse_mode=ParseMode.HTML)

    update.message.reply_text(ERROR_MESSAGE, parse_mode=ParseMode.MARKDOWN)


def main():
    updater = Updater(BOT_TOKEN, use_context=True)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("today", check_events_day))
    dispatcher.add_handler(CommandHandler("month", check_events_month))
    dispatcher.add_handler(CommandHandler("notifications", toogle_user_notifications))
    dispatcher.add_handler(CommandHandler("statistics", send_bot_statistics))
    dispatcher.add_handler(MessageHandler(Filters.command, not_found_command))

    dispatcher.add_error_handler(error)

    updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    main()
