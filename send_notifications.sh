#!/bin/bash

while getopts t: flag
do
    case "${flag}" in
        t) type=${OPTARG};;
    esac
done

docker exec -i $(docker ps -q --filter "name=astronomicalevents-notifications") python main.py $type
