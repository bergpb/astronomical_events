from loguru import logger
from config import Config

from pathlib import Path

config = Config()

formatter = (
    "[{level.icon}{level:^10}] {time:YYYY-MM-DD hh:mm:ss} {file} - {name}: {message}"
)

try:
    level = config.LOGGER_LEVEL
except AttributeError:
    level = "INFO"
try:
    file_name = config.LOGGER_FILE
except AttributeError:
    file_name = "astronomical_events.log"

log_path = Path("..") / file_name

logger.add(log_path, format=formatter, level=level, rotation="500 MB", colorize=True)
