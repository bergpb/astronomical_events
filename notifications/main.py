from sys import argv, exit
from logger import logger
from utils import send_daily_messages, send_daily_events, send_monthly_events

try:
    mode = argv[1]
except IndexError:
    logger.warning("Please, pass if is notifications for daily or monthly.")
    exit(1)

if mode == "daily":
    send_daily_messages()
    send_daily_events()

elif mode == "monthly":
    send_monthly_events()

else:
    logger.warning("Wrong argument!")
