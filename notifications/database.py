import os
import psycopg2
from logger import logger
from config import Config
from psycopg2.extras import RealDictCursor

config = Config()


class Database:
    def open_connection(self):
        self.conn = None

        try:
            self.conn = psycopg2.connect(
                host=config.HOST,
                database=config.DATABASE,
                user=config.USERNAME,
                password=config.PASSWORD,
            )

        except (Exception, psycopg2.DatabaseError) as error:
            logger.info(f"Oops! Fail to connect with database: {type(error)}: {error}")

    def create_table_notification(self):
        """Create tables in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """
                        CREATE TABLE IF NOT EXISTS notifications (
                            id SERIAL PRIMARY KEY,
                            content TEXT,
                            is_sended BOOL DEFAULT 'f',
                            sended_at TIMESTAMP,
                            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                            updated_at TIMESTAMP
                        );
                    """
                )
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            logger.info(
                f"Oops! Fail to create table notifications: {type(error)}: {error}"
            )

    def get_daily_messages(self):
        self.open_connection()
        try:
            with self.conn.cursor(
                cursor_factory=psycopg2.extras.NamedTupleCursor
            ) as cur:
                cur.execute("select id, content from notifications where is_sended='f'")
                messages = cur.fetchall()
            self.conn.close()
            return messages

        except Exception as error:
            logger.info(f"Oops! Fail to get_messages: {type(error)}: {error}")

    def get_all_users(self):
        self.open_connection()
        try:
            with self.conn.cursor(
                cursor_factory=psycopg2.extras.NamedTupleCursor
            ) as cur:
                cur.execute(
                    "select chat_id, firstname from users where receive_notifications='t'"
                )
                all_users = cur.fetchall()
            self.conn.close()
            return all_users

        except Exception as error:
            logger.info(f"Oops! Fail to get all users: {type(error)}: {error}")

    def return_all_events_for_day(self, day, month, year):
        """Select events filtering by date"""
        self.open_connection()
        try:
            with self.conn.cursor(
                cursor_factory=psycopg2.extras.NamedTupleCursor
            ) as cur:
                cur.execute(
                    """SELECT id, title, description, dates, month, year
                    FROM events
                    WHERE %s = ANY (dates)
                    AND month = %s
                    AND year = %s;""",
                    (
                        day,
                        month,
                        year,
                    ),
                )
                results = cur.fetchall()
            self.conn.close()
            return results

        except Exception as error:
            logger.info(
                f"Oops! Fail to return all events by day: {type(error)}: {error}"
            )

    def return_all_events_for_month(self, month, year):
        """Select events filtering by month"""
        self.open_connection()
        try:
            with self.conn.cursor(
                cursor_factory=psycopg2.extras.NamedTupleCursor
            ) as cur:
                cur.execute(
                    """SELECT id, title, dates, month, year
                    FROM events
                    WHERE month = %s
                    AND year = %s;""",
                    (
                        month,
                        year,
                    ),
                )
                results = cur.fetchall()
            self.conn.close()
            return results

        except Exception as error:
            logger.info(
                f"Oops! Fail to return all events for month: {type(error)}: {error}"
            )

    def insert_message(self, content):
        """Insert message to be send in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """insert into notifications (content) values(%s)""",
                    (content,),
                )

                self.conn.commit()
            self.conn.close()

        except Exception as error:
            logger.info(f"Oops! Fail to insert message: {type(error)}: {error}")

    def mark_notification_with_sended(self, id, datetime):
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute(
                    """update notifications set is_sended='t', sended_at=(%s) where id=(%s)""",
                    (datetime, id),
                )
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            logger.info(
                f"Oops! Fail to mark notification with sended: {type(error)}: {error}"
            )

    def drop_tables(self):
        """Drop tables in database"""
        self.open_connection()
        try:
            with self.conn.cursor() as cur:
                cur.execute("""DROP TABLE IF EXISTS notifications;""")
                self.conn.commit()
            self.conn.close()

        except Exception as error:
            logger.info(
                f"Oops! Fail to drop table notification: {type(error)}: {error}"
            )


if __name__ == "__main__":
    db = Database()
    db.drop_tables()
    db.create_table_notification()
    db.insert_message("This is a test message")
