# Astronomical Events

Flask app and Telegram Bot to scrapy, and send astronomical calendar of celestial events from http://www.seasky.org.

This project is composed of 3 applications:
1. Web - Provide API/Admin to be used from bot and administrator
2. Bot - Telegram bot to interact with users
3. Notifications - Service to send notifications into users using Telegram (CronJob)

Requirements: `make`, `docker` and `docker compose (v2)`.

## How to run:
1. Clone repository,
2. Copy .env.example into a .env in api and bot folders, changing variables to access database,
3. Run command `make`, to build and start containers (The database will be created automatically with the sql file located in `db` folder),
5. Run command `make scrapy`, to scrapy and save data in database from Seasky site,
6. Now all the apps have the necessary data to answer bot commands and API requests

### Deploy with Ansible:
1. Install [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html),
2. Configure ssh keys into your host and remote host,
3. Create a hosts file with your remote host ip and group,
4. Deploy with command: `ansible-playbook -i hosts playbook.yml`

### Sending user notifications with crontab:
1. Install `python-crontab`: `pip3 install python-crontab`,
2. Run `create_crontab_entries.py` script: `python3 create_crontab_entries.py`,
3. Give execution permissions into script `chmod +x send_notifications.sh`.

### Running `gitlab-ci` locally:
1. Define env variables HOSTS with hosts to deploy separated by comma, and SSH_PRIVATE_KEY, to connect with remote host.
2. `gitlab-runner exec docker --env HOSTS=$HOSTS --env SSH_PRIVATE_KEY=$SSH_PRIVATE_KEY deploy`

### Pushing containers to registry:
1. Login with Gitlab Container Registry,
2. Run the command for the respective service:
    - `make SERVICE=app TAG=v0.1 multiarch`
    - `make SERVICE=bot TAG=v0.1 multiarch`
    - `make SERVICE=notifications TAG=v0.1 multiarch`

### Backup/Restore database:
1. `make db-backup`
2. `make db-restore`