import re
import requests
from datetime import datetime as dt
from bs4 import BeautifulSoup
from ext.database import Database
from ext.utils import (
    return_image,
    return_number_of_month,
)

data = []
db = Database()


def run_scrapy(current_year):
    url = f"http://www.seasky.org/astronomy/astronomy-calendar-{current_year}.html"
    html_doc = requests.get(url).content
    soup = BeautifulSoup(html_doc, "html.parser")
    container = soup.find_all(id="right-column-content")[1]

    for item in container.find_all("li", class_=re.compile("b*")):
        month_and_date = item.find("span", class_="date-text").text
        month_full_name = month_and_date.split(" ")[0]
        month = return_number_of_month(month_full_name)
        dates = [int(item.replace(",", "")) for item in month_and_date.split(" ")[1:] if item != '']
        title = item.find("span", class_="title-text").text.replace(".", "")
        all_description = item.find("span", class_="title-text").next_sibling.strip()
        description = all_description[:-2] if all_description[-2:] == " (" else all_description
        image = return_image(item["class"][0])

        data.append((title, description, image, dates, month, current_year))

    db.insert_data(data)
    print(f"Scrapy for {current_year} finished.")
