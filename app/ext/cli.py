import click
from ext.scrapy import run_scrapy
from ext.database import Database
from datetime import datetime as dt


def init_app(app):
    @app.cli.command("scrapy")
    @click.argument("current_year", nargs=-1)
    def scrapy_data(current_year):
        if not current_year:
            current_year = dt.now().year
        else:
            current_year = current_year[0]
        print(f"Scrapping year: {current_year}...")
        run_scrapy(current_year)

    @app.cli.command("db-migrate")
    def create_table():
        """Create table in database."""
        db = Database()
        db.create_table_events()

    @app.cli.command("db-drop")
    def drop_table():
        """Drop table in database."""
        db = Database()
        db.drop_table()
