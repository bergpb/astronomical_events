from flask import render_template, request
from ext.database import Database as DB
from ext.config import Config
from flask_paginate import Pagination, get_page_parameter

from . import users

db = DB()
config = Config()


@users.route("/users", methods=["GET"])
def list():
    limit = int(config.ITENS_PER_PAGE)
    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (page*limit) - limit

    users_total = db.count_users()
    users = db.return_all_users(limit, offset)

    pagination = Pagination(page=page, page_per=limit, total=users_total)

    return render_template(
        "users/list.html",
        pagination=pagination,
        users=users
    )
