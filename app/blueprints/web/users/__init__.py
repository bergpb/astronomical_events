from flask import Blueprint

users = Blueprint(
    "users",
    __name__,
    static_folder="static",
    static_url_path='/static/web',
    url_prefix="/admin"
)

from . import views

def init_app(app):
    app.register_blueprint(users)