from flask import Blueprint

notifications = Blueprint(
    "notifications",
    __name__,
    static_folder="static",
    static_url_path='/static/web',
    url_prefix="/admin"
)

from . import views

def init_app(app):
    app.register_blueprint(notifications)