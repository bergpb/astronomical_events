# Astronomy Events
Tool to scrapy, save and make a api from astronomy calendar of celestial events from http://www.seasky.org.

Requirements: ```docker```, ```docker-compose``` and ```make```.

## How to run:
1. Clone repository,
2. Copy .env.example into a .env changing variables to access database,
3. Run command ```make up```, to start containers,
4. Run command ```make create-table``` to create table,
5. Run command ```make scrapy```, to start get and save data,
6. Access [http://localhost:5000](http://localhost:5000). Obs: default route return 404 Not Found.

### Some endpoints:
[http://localhost:5000/api/v1/filter_by?day=30](http://localhost:5000/api/v1/filter_by?day=30) -> Get events by date (current month).  
[http://localhost:5000/api/v1/filter_by?month=12](http://localhost:5000/api/v1/filter_by?month=12) -> Get events by month (current year).

